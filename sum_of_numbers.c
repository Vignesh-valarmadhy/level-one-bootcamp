//Write a program to find the sum of n different numbers using 4 functions

#include <stdio.h>
#include <conio.h>

int input()
{
    int a[21], size;

	printf("Enter the size of numbers you want to add: \n");
	scanf("%d",&size);

	printf("Enter %d elements to add:\n",size);
	for (int i = 0; i < size; i++)
	{
	    scanf("%d",&a[i]);
    }
    
    execute(a,size);
}

int execute(int a[],int size)
{
    int add = 0;
    for (int i = 0; i<size; i++)
    {
    	add = add + a[i];
    }
    
    display(add);
}


int display(int add)
{
    printf("The Sum of All The Given Numbers After Addition = %d\n",add);
}

int main()
{
    input();
}