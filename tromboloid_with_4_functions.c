#include <stdio.h>
#include <conio.h>
 
float volume_of_tromboloid(float h, float b, float d)
{
   return ((h * d * b) + ( d / b)) / 3;
}
 
 float input()
{
   float temp;
   scanf("%f",&temp);
   return temp;
}
 
 
void output(float h, float b, float d, float volume)
{
printf("The Volume of the tromboloid when Height %f, Breadth %f and Depth %f is: %2f\n", h,b,d,volume);
}
 
 
int main()
{
   float h,b,d,a,volume;
   printf("Enter Height of the tromboloid:\n");
   h = input();
  
   printf("Enter  Breadth of the tromboloid:\n");
   b = input();
  
   printf("Enter  Depth of the tromboloid:\n");
   d = input();
  
   volume = volume_of_tromboloid(h, b, d);
  
   output(h,b,d,volume);
  
   return 0;
}
