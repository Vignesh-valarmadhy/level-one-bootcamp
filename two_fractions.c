
//WAP to find the sum of two fractions.
#include<stdio.h>
struct Fraction
{
	int numerator,denominator;
}frac1,frac2;


int Add (int frac1Numerator, int frac1Denominator, int frac2Numerator, int frac2Denominator)
{
	int num, den, GCD;
		
	num = (frac1Numerator * frac2Denominator) + (frac1Denominator * frac2Numerator);

	den = frac1Denominator * frac2Denominator;

	GCD = gcd(num,den);

int num1 = num/GCD;
int den1 = den/GCD;

output(frac1Numerator, frac1Denominator, frac2Numerator, frac2Denominator, num1, den1);
}



int input()
{
	printf("For The First Fraction:\n");
	printf("Enter the Numerator Value:\n");
	scanf("%d",&frac1.numerator);
	printf("Enter the denominator Value:\n");
	scanf("%d",&frac1.denominator);

	printf("For The Second Fraction:\n");
	printf("Enter the Numerator Value:\n");
	scanf("%d",&frac2.numerator);
	printf("Enter the denominator Value:\n");
	scanf("%d",&frac2.denominator);
}

int output(int f1Numerator, int f1Denominator, int f2Numerator, int f2Denominator, int Num, int Den)
{
	printf("The addition of two fractions when the First Fraction is %d/%d and the Second Fraction is %d/%d is: %d/%d\n", f1Numerator, f1Denominator, f2Numerator, f2Denominator, Num, Den);
}

int gcd (int Num, int Den)
{
	if (Den==0)
	{
		return Num;
	}
	
	return gcd(Den, Num % Den);
}



int main()
{
	input();
	Add (frac1.numerator, frac1.denominator,frac2.numerator, frac2.denominator);
	return 0;
}
