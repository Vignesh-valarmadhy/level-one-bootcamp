//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>

struct Point
{
    float x,x1,y,y1;
}m,n;

float output(float firstP,float secondP,float Distance)
{
    printf("The distance between the two points %f and %f is: %f\n", firstP, secondP,Distance);
}

float compute()
{
    float firstP,secondP,distance;
    firstP = n.x1 - m.x;
    secondP = n.y1 - m.y;
    distance = sqrt(firstP*firstP + secondP*secondP);
    output(firstP,secondP,distance);
    return distance;
}



float input()
{
    printf(" The First point:\n");
    printf("Enter The Value for X:\n");
    scanf("%f",&m.x);
    printf("Enter the Value For Y:\n");
    scanf("%f",&n.y);
    
    printf(" The Second point:\n");
    printf("Enter the Value For X:\n");
    scanf("%f",&m.x1);
    printf("Enter the Value For Y:\n");
    scanf("%f",&n.y1);
}




int main()
{
    input();
    compute();
    
    return 0;
}