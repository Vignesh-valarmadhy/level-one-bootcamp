
#include <stdio.h>
#include<math.h>

float distance(float a,float b)
{
    float result;
  
	result = sqrt(a*a + b*b);
	return result;
}   

float input()
{
    float k;
    scanf("%f",&k);
    return k;
}

void output(float x, float y, float output)
{
    printf("Distance between the points %f and %f is %2f",x,y,output);
}

int main()
{
    float a, a2, b, b2,x,y,dist;
    printf(" First point:\n");
    printf("Enter The Value for X:\n");
    a = input();
    printf("Enter the Value For Y:\n");
    b = input();
    
    printf(" The Second point:\n");
    printf("Enter the Value For X:\n");
    a2 = input();
    printf("Enter the Value For Y:\n");
    b2 = input();
    
    x = (a2 - a);
    y = (b2 - b);
    
    dist = distance(x, y);
    output(x,y,dist);

    return 0;
}