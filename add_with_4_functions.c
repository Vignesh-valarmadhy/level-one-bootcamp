
#include<stdio.h>

int first_number()
{
	int num;
	printf("Enter the first number: \n ");
	scanf("%d",&num);
	return num;
}

int second_number()
{
	int num;
	printf("Enter the second number: \n ");
	scanf("%d",&num);
	return num;
}

int add_two_numbers(int first_num,int second_num)
{ 
	return (first_num+second_num);
}


int main()
{
	int a=first_number();
	int b=second_number();
	int sum=add_two_numbers(a,b);
	printf("The Sum of the two numbers %d and %d is = %d\n",a,b,sum);
	return 0;
}

